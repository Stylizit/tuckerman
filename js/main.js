$(function () {
    var $header = $('#header .row-nav');
    var $burger = $('#menu-btn');
    var $hero = $('#hero-image');
    var $slideshow = $('.slideshow');
    var $toscroll = $('.scroll-section');
    
    var heroHeight = calculateHeroHeight();
    var isFlickity = false;
    
    initBurgerMenu();
    initSmoothScroll();
    
    //$(window).on('load', function () {
    if (window.innerWidth > 1024) {
        attachScrollListener();
        initSlideshow();
    }
        
    //});
    
    $(window).on('resize', function () {
        heroHeight = calculateHeroHeight();
        
        if (window.innerWidth > 1024) {
            attachScrollListener();
            initSlideshow();
        } else {
            detachScrollListener();
            destroySlideshow();
        }
    });
    
    
    function attachScrollListener () {
        $(window).on('scroll', function () {
            var scrollTop = $(window).scrollTop();

            if (scrollTop > heroHeight / 2) {
                $('.header').addClass('header-small');
            } else {
                $('.header').removeClass('header-small');
            }
        });
    }
    
    function detachScrollListener () {
        $(window).off('scroll');
    }
    
    
    
    function calculateHeroHeight () {
        return window.innerHeight - $header.height();
    }
    
    function initBurgerMenu() {
        $burger.on('click', function () {
            $(this).toggleClass('burger-active'); 
        });
    }
    
    function initSmoothScroll () {
        $toscroll.on('click', function (e) {
            e.preventDefault();
            var scrollVal = $($(this).attr('href')).offset().top;
            
            $('html, body').animate({
                scrollTop: scrollVal - $header.height()
            }, 'slow');
        });
    }
    
    function initSlideshow() {
        $hero.css({
            'height': heroHeight + 'px'
        });
        
        if (!isFlickity) {
            $slideshow.flickity({
                pageDots: false,
                wrapAround: true,
                arrowShape: { 
                  x0: 40,
                  x1: 55, y1: 15,
                  x2: 50, y2: 20,
                  x3: 30
                }
            });
            
            isFlickity = true;
        } 
        
        $('.flickity-viewport').css({
            'height': heroHeight + 'px'
        });
    }
    
    function destroySlideshow () {
        $hero.css({
            'height': 'auto'
        });
        
        if (isFlickity) {
            $slideshow.flickity('destroy');
        }
        
        isFlickity = false;
        
        $('.flickity-viewport').css({
            'height': 'auto'
        });
    }
});